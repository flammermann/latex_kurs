[Markdown Anleitung](https://guides.github.com/features/mastering-markdown/)

# LaTeX-Kurs

Dieses Repository enthält alle Vorlesungen des LaTeX-Kurses des Departments Physik an der Friedrich-Alexander-Universität Erlangen-Nürnberg aus dem Sommersemester 2019.

## Glossary

Im Glossar werden alle Befehle, die in der Vorlesung vorkommen, gesammelt und kurz beschrieben. Das Glossar wird zu jeder Vorlesung aktualisiert, so dass es alle bisher behandelten Befehle enthält.

## Lectures

Enhält die .tex und .pdf Dateien der Vorlesung. Die Ordner `Vorlesung[Nummer]` enthalten jeweils den .tex Code der entsprechenden Vorlesung. Allgemeine Struktur, Logos und getexte Beispiele sind in extra Ordnern. Fuehrt man das Skript `Compile [Nummer]` werden alle relevanten Files in den entsprechenden Vorlesungsordner kopiert und die .tex Datei wird kompiliert. Nach dem Kompilieren werden alle Metafiles wieder entfernt. Das Skript `CreateZIP [Nummer]` erstellt eine .zip Datei mit allen Inhalten, die gebraucht werden um die .tex Datei zu kompilieren. Diese .zip Datei wird dann im Studon-Kurs zur Verfügung gestellt. Moechte man nicht immer per Skript kompilieren, kann man das Skript `RefreshForManualCompile [Nummer]` ausführen. Dann werden alle relevanten Files dauerhaft in das entsprechende Verzeichnis kopiert und man kann einfach mit seinem .tex Editor kompilieren. `CleanUp [Nummer]` kann dann genutzt werden um die Ordner wieder aufzuräumen.

### Examples

### Struktur

### Logos


## Overview

Der rote Fade beschreibt die Struktur der Vorlesung und gibt Beispiele. Er wird den Teilnehmenden direkt zu Beginn des Kurses zur Verfügung.
