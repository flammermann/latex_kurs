\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{LaTeX-Kurs}[2017/10/22 LaTeX-Kurs WiSe2017 Klasse]

\LoadClass[11pt]{beamer}
\ClassInfo{LaTeX-Kurs}{Dies ist die Klasse, welche zum Erstellen der Vorlesungsfolien für den LaTeX-Kurs im Winteresemester 2017/18 genutzt wird bzw. wurde. Diese Klasse wurde das letzte Mal am 22. Oktober 2017 bearbeitet.}
\ClassWarning{LaTeX-Kurs}{Die block-, exampleblock- und alertblock-Umgebung wurden vollständig überschrieben. Dies hat zur Folge, dass die Einschränkung auf einzelne Instanzen mit spitzen Klammern hinter der Umgebung nicht mehr funktioniert. Will man einen Block nur auf einzelnen Instanzen anzeigen, muss man auf den only-, visible- oder uncover-Befehl ausweichen.}

% \ClassError{class-name}{error-text}{help-text}
% \ClassWarning{class-name}{warning-text}
% \ClassWarningNoLine{class-name}{warning-text}

\RequirePackage{etex}

\RequirePackage[english,ngerman]{babel}
\RequirePackage{ngerman}

\RequirePackage[utf8]{inputenc}
\RequirePackage{changepage}

\RequirePackage[T1]{fontenc}
\RequirePackage{LobsterTwo}
\RequirePackage{frcursive}
\RequirePackage{comicneue}
\RequirePackage{comfortaa}
\RequirePackage{bookman}
\RequirePackage{crimson}
\RequirePackage{times}
\RequirePackage{helvet}
\RequirePackage{lmodern}
\RequirePackage{textcomp}

\RequirePackage{amsmath}
\RequirePackage{amsfonts}
  \DeclareMathAlphabet\EuRoman{U}{eur}{m}{n}
  \SetMathAlphabet\EuRoman{bold}{U}{eur}{b}{n}
\RequirePackage{amssymb}
  \let\checkmark\undefined
\RequirePackage{nicefrac}
\RequirePackage{wasysym}
  \let\Square\undefined
\RequirePackage{MnSymbol}
  \let\checkmark\undefined
\RequirePackage{mathtools}
\RequirePackage{siunitx}

\DeclareMathVersion{manyfontsononeslide} % Braucht man, weil LaTeX sonst einen Fehler auf der Mathe-Microtypographie-Folie wirft, weil er findet, dass es zu viele Mathe Schriftarten sind...

\RequirePackage{pifont}
\RequirePackage{bbding}
\RequirePackage[normalem]{ulem}
\RequirePackage{soul}
\RequirePackage{dingbat}
\RequirePackage{lipsum}
\RequirePackage{relsize}

\RequirePackage{multicol}
\RequirePackage{multirow}
\RequirePackage{booktabs}
\RequirePackage{colortbl}
\RequirePackage{array}
\RequirePackage{tabularx}

\RequirePackage{listings}
\RequirePackage{tcolorbox}
  \tcbuselibrary{skins,hooks}

\RequirePackage{graphicx}
  \DeclareGraphicsExtensions{.png,.jpg,.pdf}
  \graphicspath{%
    {Figures/Pictures/},%
    {Figures/Documents/},%
    {Figures/Documents/Beamer/},%
    {Figures/Logos/},%
  }
\RequirePackage{pdfpages}

\RequirePackage{transparent}
\RequirePackage{xcolor}

\RequirePackage{microtype}
\RequirePackage{ragged2e}
\RequirePackage[autostyle]{csquotes}

\RequirePackage{xparse}
\RequirePackage{xpatch}
\RequirePackage{xspace}
\RequirePackage[useregional]{datetime2}

\RequirePackage{tikz}
  \usetikzlibrary{arrows,shapes,ocgx,backgrounds,patterns,decorations.pathmorphing}
\RequirePackage{pgfplots}

\RequirePackage{hanging}

\RequirePackage{ocgx}

\RequirePackage{hyperref}

\RequirePackage{etoolbox}

%Commands

\newcommand{\killDepth}[1]{\raisebox{0pt}[\height][0pt]{#1}}

\def\zenter{\par\begingroup\Centering}
\def\endzenter{\par\endgroup}

\newcommand{\ra}{$\rightarrow$~}
\newcommand{\al}{\glqq}
\newcommand{\ar}{\grqq}
\newcommand{\emptyline}{\textcolor{gray}{\([\)\hfill\(]\)}}
\newcommand{\emptyspace}{\textcolor{gray}{\textvisiblespace}}

\newcommand{\TikZ}{{\rmfamily Ti{\itshape k}Z}\xspace}
\newcommand{\pgfPlots}{{\rmfamily{\scshape pgf}Plots}\xspace}
\newcommand{\pgfPlotsTable}{{\rmfamily{\scshape pgf}PlotsTable}\xspace}
\newcommand{\pgf}{{\rmfamily\scshape pgf}\xspace}

\newcommand{\smallnote}[1]
	{{\centering\tiny\vskip0.5\baselineskip\myGray{#1}\vskip0.5\baselineskip\normalsize\par}}

\NewDocumentCommand{\iurl}{moo}{%
  \href{http://#1}{%
    \IfNoValueTF{#3}{\ttfamily}{#3}%
    \IfNoValueTF{#2}{#1}{#2}%
  }\xspace%
}
\NewDocumentCommand{\murl}{moo}{%
  \href{mailto:#1}{%
    \IfNoValueTF{#3}{\ttfamily }{#3}%
    \IfNoValueTF{#2}{#1}{#2}%
  }\xspace%
}

\NewDocumentCommand{\roundedImage}{O{}mO{1.5mm}}{%
  \begin{tikzpicture}[baseline]%
    \transparent{0}
    \node [inner sep=0pt, anchor=center] (center) at (0,0) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
    \transparent{1}
    \clip [rounded corners=#3]%
      (current bounding box.north west) rectangle (current bounding box.south east);%
    \node [inner sep=0pt, anchor=center] at (center) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
  \end{tikzpicture}%
}
\NewDocumentCommand{\roundedPDF}{O{}mO{1.5mm}}{%
  \begin{tikzpicture}[baseline]%
    \transparent{0}
    \node [inner sep=0pt, anchor=center, fill=white] (center) at (0,0) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
    \transparent{1}
    \clip [rounded corners=#3]%
      (current bounding box.north west) rectangle (current bounding box.south east);%
    \node [inner sep=0pt, anchor=center, fill=white] at (center) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
  \end{tikzpicture}%
}

\NewDocumentCommand{\invroundedPDF}{O{}mmmmm}{%
  \begin{tikzpicture}[baseline]%
    \transparent{0}
    \node [inner sep=0pt, anchor=center, fill=white] (center) at (0,0) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
    \transparent{1}
    \clip %
    (current bounding box.north west)[#3] --
    (current bounding box.north east)[#4] --
    (current bounding box.south east)[#5] --
    (current bounding box.south west)[#6] -- cycle;%
    \node [inner sep=0pt, anchor=center, fill=white] at (center) {\normalcolor\includegraphics[width=1\linewidth,clip,keepaspectratio,#1]{#2}};%
  \end{tikzpicture}
}

\newcommand{\super}{Su"-per"-ca"-li"-fra"-gi"-lis"-tic"-ex"-pi"-ali"-ge"-tisch}
\newcommand{\rightloop}{\rotatebox[origin=c]{180}{$\looparrowleft$}}

\newcommand{\Arg}[2][verb]{\ifthenelse{\equal{#1}{verb}}{\textcolor{gray}{\,\textit{#2}\,}}{\textcolor{gray}{\textit{#2}}}}
\newcommand{\blueArg}[2][verb]{\ifthenelse{\equal{#1}{verb}}{\blue{\,\textit{#2}\,}}{\blue{\textit{#2}}}}
\newcommand{\CommentSign}{\,\texttt{\color{myBlack50}\%}}
\ExplSyntaxOn
  \NewDocumentCommand{\blueVerb}{v}{\blue{\texttt{#1}}}
  \NewDocumentCommand{\alertVerb}{v}{\alert{\texttt{#1}}}
  \NewDocumentCommand{\grayVerb}{v}{\textcolor{myBlack50}{\texttt{#1}}}
\ExplSyntaxOff
\newcommand{\Star}[1][bracket]{\smash{\ifthenelse{\equal{#1}{bracket}}{$^\texttt{(*)}$}{$^\texttt{*}$}}}
\newcommand{\blueStar}[1][bracket]{\smash{\ifthenelse{\equal{#1}{bracket}}{$^\texttt{\blue{(*)}}$}{$^\texttt{\blue{*}}$}}}

\newcommand{\starred}{\raisebox{-0.1em}{\textsuperscript{\smash{\texttt{*}}}}}
\newcommand{\optstarred}{\raisebox{-0.1em}{\textsuperscript{\smash{\texttt{(\hskip-0.1em*\hskip-0.1em)}}}}}

\newcommand{\niceframe}[1]{\raisebox{-\depth}{\vphantom{#1}\color{tcbcolframe}\frame{\textcolor{fg}{#1}}}}

%Environments

\newenvironment{textbox}[1]
	{\begin{beamerboxesrounded}[upper=white, lower=white, shadow=true]{#1}}
	{\end{beamerboxesrounded}}

\newenvironment{pageblock}[1]
	{\begin{center}\begin{minipage}[c]{#1\textwidth}}
	{\end{minipage}\end{center}}

\newenvironment{nospacecenter}
	{\vspace{-1em}\begin{center}}
	{\end{center}\vspace{-1em}}

%Special

\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{/}{!{\vrule width \heavyrulewidth}}

\setlength{\parindent}{0pt}

\newcommand{\BibTeX}{B\hspace{-0.025em}ib\hspace{-0.15em}\TeX}

%Neue Farben definieren

%   \definecolor	{myOriginalRed}	{HTML}{A3061E}
%     \colorlet	{myRed}		{myOriginalRed!75}\newcommand{\myRed}[1]{\textcolor{myRed}{#1}}
  \definecolor	{myRed}	{HTML}{D46C7C}\newcommand{\myRed}[1]{\textcolor{myRed}{#1}}
    \colorlet	{myRed90}	{myRed!90}
    \colorlet	{myRed80}	{myRed!80}
    \colorlet	{myRed70}	{myRed!70}
    \colorlet	{myRed60}	{myRed!60}
    \colorlet	{myRed50}	{myRed!50}\newcommand{\myLightRed}[1]{\textcolor{myRed50}{#1}}
    \colorlet	{myRed40}	{myRed!40}
    \colorlet	{myRed30}	{myRed!30}
    \colorlet	{myRed20}	{myRed!20}
    \colorlet	{myRed10}	{myRed!10}

%   \definecolor	{myOriginalYellow}	{HTML}{FFBC42}
%     \colorlet	{myYellow}	{myOriginalYellow!75}\newcommand{\myYellow}[1]{\textcolor{myYellow}{#1}}
  \definecolor	{myYellow}	{HTML}{F8CF70}\newcommand{\myYellow}[1]{\textcolor{myYellow}{#1}}
    \colorlet	{myYellow90}	{myYellow!90}
    \colorlet	{myYellow80}	{myYellow!80}
    \colorlet	{myYellow70}	{myYellow!70}
    \colorlet	{myYellow60}	{myYellow!60}
    \colorlet	{myYellow50}	{myYellow!50}\newcommand{\myYellowGreen}[1]{\textcolor{myYellow50}{#1}}
    \colorlet	{myYellow40}	{myYellow!40}
    \colorlet	{myYellow30}	{myYellow!30}
    \colorlet	{myYellow20}	{myYellow!20}
    \colorlet	{myYellow10}	{myYellow!10}

%   \definecolor	{myOriginalBlue}	{HTML}{003F77}
%     \colorlet	{myBlue}	{myOriginalBlue!75}\newcommand{\myBlue}[1]{\textcolor{myBlue}{#1}}\newcommand{\blue}[1]{\myBlue{#1}}
  \definecolor	{myBlue}	{HTML}{3b7bb5}\newcommand{\myBlue}[1]{\textcolor{myBlue}{#1}}\newcommand{\blue}[1]{\myBlue{#1}}
    \colorlet	{myBlue90}	{myBlue!90}
    \colorlet	{myBlue80}	{myBlue!80}
    \colorlet	{myBlue70}	{myBlue!70}
    \colorlet	{myBlue60}	{myBlue!60}
    \colorlet	{myBlue50}	{myBlue!50}\newcommand{\myLightBlue}[1]{\textcolor{myBlue50}{#1}}
    \colorlet	{myBlue40}	{myBlue!40}
    \colorlet	{myBlue30}	{myBlue!30}
    \colorlet	{myBlue20}	{myBlue!20}
    \colorlet	{myBlue10}	{myBlue!10}

  \definecolor	{myBlack}{HTML}{0B132B}\newcommand{\myBlack}[1]{\textcolor{myBlack}{#1}}
    \colorlet	{myBlack90}	{myBlack!90}
    \colorlet	{myBlack80}	{myBlack!80}
    \colorlet	{myBlack70}	{myBlack!70}
    \colorlet	{myBlack60}	{myBlack!60}
    \colorlet	{myBlack50}	{myBlack!50}\newcommand{\myGray}[1]{\textcolor{myBlack50}{#1}}
    \colorlet	{myBlack40}	{myBlack!40}
    \colorlet	{myBlack30}	{myBlack!30}
    \colorlet	{myBlack20}	{myBlack!20}
    \colorlet	{myBlack10}	{myBlack!10}

%   \definecolor	{myOriginalGreen}	{HTML}{0B6E4F}
%     \colorlet	{myGreen}	{myOriginalGreen!75}\newcommand{\myGreen}[1]{\textcolor{myGreen}{#1}}
  \definecolor	{myGreen}{HTML}{43997e}\newcommand{\myGreen}[1]{\textcolor{myGreen}{#1}}
    \colorlet	{myGreen90}	{myGreen!90}
    \colorlet	{myGreen80}	{myGreen!80}
    \colorlet	{myGreen70}	{myGreen!70}
    \colorlet	{myGreen60}	{myGreen!60}
    \colorlet	{myGreen50}	{myGreen!50}\newcommand{\myLightGreen}[1]{\textcolor{myGreen50}{#1}}
    \colorlet	{myGreen40}	{myGreen!40}
    \colorlet	{myGreen30}	{myGreen!30}
    \colorlet	{myGreen20}	{myGreen!20}
    \colorlet	{myGreen10}	{myGreen!10}
  
  \colorlet	{myOrange}	{myYellow!60!myRed}
  \colorlet	{myPurple}	{myRed!50!myBlue!80}

  \definecolor	{milka}			{HTML}{7D68AC}
  \definecolor	{darkmilka}		{HTML}{49306b}
  \definecolor	{platinum}		{HTML}{e9e3e6}
  \definecolor	{lightpink}		{HTML}{ebbab9}
  
  \definecolor	{wordblue}		{HTML}{4f81bd}
  \definecolor	{wordlightblue}		{HTML}{95b3d7}
  \definecolor	{wordsuperlightblue}	{HTML}{dce6f1}
  \definecolor	{wordblack}		{HTML}{4f81bd}
  \definecolor	{wordlightblack}	{HTML}{95b3d7}
  \definecolor	{wordsuperlightblack}	{HTML}{dce6f1}
  \definecolor	{wordred}		{HTML}{c0504d}

  \definecolor	{HeNe}			{wave}{632.8}
  \definecolor	{unleserlich}		{gray}{0.8}
  \definecolor	{kotzgruen}		{rgb}{0.5,0.5,0}
  \definecolor	{uluru}			{RGB}{250,102,31}
  \colorlet	{hellkotzgruen}		{kotzgruen!50}
  \colorlet	{mischmasch}		{kotzgruen!10!milka!70!HeNe}

  \definecolor	{farbe1}		{HTML}{FBF2C0}
  \definecolor	{farbe2}		{HTML}{EDD382}
  \definecolor	{farbe3}		{HTML}{FA661F}
  \definecolor	{farbe4}		{HTML}{FC9e4F}
  \definecolor	{farbe5}		{HTML}{48392A}
  
  \definecolor	{babyrosa}		{RGB}{255, 213, 237}
  \definecolor	{babyblau}		{RGB}{203, 229, 252}
  
\newcommand{\gray}[1]{\textcolor{gray}{#1}}

\renewcommand{\smallskip}{\vskip0.2\baselineskip}

%Neue Befehle für Titelseite, Inhaltsverzeichnis, Abschnitte und Unterabschnitte

  \NewDocumentCommand{\titleframe}{}{%
    {% Diese Zeile ist wichtig, damit Hintergrund und Fußzeile nur für diesen Frame angepasst werden.
      \setbeamertemplate{footline}{}%
      \setbeamertemplate{background canvas}{\includegraphics[height=\paperheight]{fausiegel}}%
      \begin{frame}%
	\begin{dateblock}
	 \killDepth{\small\bfseries\insertdate}
	\end{dateblock}
	\begin{titleblock}
	  \killDepth{\Large\bfseries\inserttitle}\\[0.25\baselineskip]
	  \killDepth{\large\insertsubtitle}
	\end{titleblock}
	\begin{authorblock}
	 \killDepth{\large\bfseries\insertauthor}
	\end{authorblock}
	\begin{instituteblock}
	 \killDepth{\small\insertinstitute}
	\end{instituteblock}
      \end{frame}%
    }%
  }%

  \NewDocumentCommand{\tocframe}{O{}O{}O{}O{}O{}}{%
    {% Diese Zeile ist wichtig, damit Hintergrund und Fußzeile nur für diesen Frame angepasst werden.
      \setbeamertemplate{footline}{}%
      \setbeamertemplate{background canvas}{\includegraphics[height=\paperheight]{fausiegel}}%
      \begin{frame}%
      \frametitle{Inhalte}%
	\begin{contentsblock}[#1]{Inhaltsverzeichnis}%
	  \ifthenelse{\equal{#2}{}\AND\equal{#3}{}}{%
	    \tableofcontents%
	  }{%
	    \tableofcontents[sections={#2}]%
	    \setlength\columnsep{0.5\baselineskip}%
	    \begin{columns}[2][\linewidth]%
	      \tableofcontents[sections={#3}]%
	      \vskip-0.5em%
	      \columnbreak%
	      \tableofcontents[sections={#4}]%
	    \end{columns}%
	    \vskip0.5em%
	    \tableofcontents[sections={#5-}]%
	  }%
	\end{contentsblock}% 
      \end{frame}%
    }%
  }%

  \NewDocumentCommand{\mysection}{smO{}}{%
    \ifthenelse%
      {\equal{#1}{\BooleanTrue}}%
      {\section*{#2 #3}}%
      {\section{#2 #3}}%
    {% Diese Zeile ist wichtig, damit Hintergrund und Fußzeile nur für diesen Frame angepasst werden.
      \setbeamertemplate{footline}{}%
      \setbeamertemplate{background canvas}{\includegraphics[height=\paperheight]{fausiegel}}%
      \setbeamertemplate{background}{%
	\begin{tikzpicture}[even odd rule]
          \fill[myBlue] (0,0) rectangle (\paperwidth,\paperheight) (1.5mm,1.5mm) [rounded corners=0.25mm] rectangle (\paperwidth-1.5mm,\paperheight-1.5mm);
        \end{tikzpicture}
       }
      \begin{frame}%
	\begin{sectionblock}%
	  \ifthenelse%
	    {\equal{#3}{}}%
	    {\killDepth{#2}}%
	    {#2\\[0.15\baselineskip]\killDepth{#3}}%
	\end{sectionblock}%
      \end{frame}%
    }%
  }%

  \NewDocumentCommand{\mysubsection}{smO{}}{%
    \ifthenelse%
      {\equal{#1}{\BooleanTrue}}%
      {\subsection*{#2 #3}}%
      {\subsection{#2 #3}}%
    {% Diese Zeile ist wichtig, damit Hintergrund und Fußzeile nur für diesen Frame angepasst werden.
      \setbeamertemplate{footline}{}%
      \setbeamertemplate{background canvas}{\includegraphics[height=\paperheight]{fausiegel}}%
      \setbeamertemplate{background}{%
	\begin{tikzpicture}[even odd rule]
          \fill[myBlue80] (0,0) rectangle (\paperwidth,\paperheight) (1.5mm,1.5mm) [rounded corners=0.25mm] rectangle (\paperwidth-1.5mm,\paperheight-1.5mm);
        \end{tikzpicture}
       }
      \begin{frame}%
	\begin{subsectionblock}%
	  \ifthenelse%
	    {\equal{#3}{}}%
	    {\killDepth{#2}}%
	    {#2\\[0.15\baselineskip]\killDepth{#3}}%
	\end{subsectionblock}%
      \end{frame}%
    }%
  }%

  \NewDocumentCommand{\mysubsubsection}{smO{}}{%
    \ifthenelse%
      {\equal{#1}{\BooleanTrue}}%
      {\subsubsection*{#2 #3}}%
      {\subsubsection{#2 #3}}%
    {% Diese Zeile ist wichtig, damit Hintergrund und Fußzeile nur für diesen Frame angepasst werden.
      \setbeamertemplate{footline}{}%
      \setbeamertemplate{background canvas}{\includegraphics[height=\paperheight]{fausiegel}}%
      \setbeamertemplate{background}{%
	\begin{tikzpicture}[even odd rule]
          \fill[myBlue80] (0,0) rectangle (\paperwidth,\paperheight) (1.5mm,1.5mm) [rounded corners=0.25mm] rectangle (\paperwidth-1.5mm,\paperheight-1.5mm);
        \end{tikzpicture}
       }
      \begin{frame}%
	\begin{subsectionblock}%
	  \ifthenelse%
	    {\equal{#3}{}}%
	    {\killDepth{#2}}%
	    {#2\\[0.15\baselineskip]\killDepth{#3}}%
	\end{subsectionblock}%
      \end{frame}%
    }%
  }%

%Beamer-Theme

\usetheme{default}
\usefonttheme[onlymath]{serif}

\setbeamertemplate{background canvas}{}
\setbeamertemplate{navigation symbols}{}

\setbeamertemplate{footnote}{\hangpara{1em}{1}\makebox[1em][l]{\insertfootnotemark}\footnotesize\insertfootnotetext\par}

\setbeamertemplate{section in toc}{\leavevmode\leftskip=0pt\inserttocsection\par}
\setbeamertemplate{subsection in toc}{\leavevmode\leftskip=1em\inserttocsubsection\par}
\setbeamertemplate{subsubsection in toc}{\leavevmode\leftskip=2em\inserttocsubsubsection\par}

%       \setbeamertemplate{footline}{%
% 	  \leavevmode%
% 	  \hbox{%
% 	    \hspace*{5pt}%
% 	    \color{myBlack50}\switchocg{showExtendedLecture}{\killDepth{Erweiterte Vorlesung Anzeigen/Ausblenden}}%
% 	  }%
% 	  \hfill%
% 	  \hbox{%
% 	    \color{myBlack50}\killDepth{\insertframenumber{} / \inserttotalframenumber}%
% 	    \hspace*{5pt}%
% 	  }%
% 	  \vspace*{5pt}%
%       }%

\setbeamercolor{structure}{fg=white,bg=myBlue}
\setbeamercolor{item}{fg=tcbcolframe}
\setbeamercolor{alerted text}{fg=tcbcolframe}
\setbeamercolor{section in toc}{fg=myBlack}
\setbeamercolor{subsection in toc}{fg=myBlack80}

\makeatletter
\defbeamertemplate{frametitle}{mydefault}{%
  \vskip-1pt
  \begin{frametitleblock}{}%
        \killDepth{{\Large\insertframetitle}}\hfill\iurl{\StudOnKurs}[\includegraphics[height=0.351280785cm]{faulogo}]\hskip0.5\baselineskip\iurl{\StudOnKurs}[\includegraphics[height=0.351280785cm]{physiklogo}]%
        \ifx\insertframesubtitle\@empty\else{
	  \vskip-0.2\baselineskip%
	  \killDepth{\normalsize\insertframesubtitle}
	}\fi%
    \end{frametitleblock}%
}
\defbeamertemplate{frametitle}{titleoptions}[1]{%
  \vskip-1pt
  \begin{frametitleblock}[#1]{}%
    \killDepth{{\Large\insertframetitle}}\hfill\iurl{\StudOnKurs}[\includegraphics[height=0.351280785cm]{faulogo}]\hskip0.5\baselineskip\iurl{\StudOnKurs}[\includegraphics[height=0.351280785cm]{physiklogo}]%
    \ifx\insertframesubtitle\@empty\else{
      \vskip-0.2\baselineskip%
      \killDepth{\normalsize\insertframesubtitle}
    }\fi%
  \end{frametitleblock}%
}
\BeforeBeginEnvironment{frame}{%
  \setbeamertemplate{frametitle}[mydefault]%
}
\define@key{beamerframe}{titleoptions}[true]{%
  \setbeamertemplate{frametitle}[titleoptions]{#1}%
}
\makeatother

\makeatletter
\renewcommand{\itemize}[1][]{%
  \beamer@ifempty{#1}{}{\def\beamer@defaultospec{#1}}%
  \ifnum \@itemdepth >2\relax\@toodeep\else
    \advance\@itemdepth\@ne
    \beamer@computepref\@itemdepth% sets \beameritemnestingprefix
    \usebeamerfont{itemize/enumerate \beameritemnestingprefix body}%
    \usebeamercolor[fg]{itemize/enumerate \beameritemnestingprefix body}%
    \usebeamertemplate{itemize/enumerate \beameritemnestingprefix body begin}%
    \list
      {\usebeamertemplate{itemize \beameritemnestingprefix item}}
      {\def\makelabel##1{%
          {%
            \hss\llap{{%
                \usebeamerfont*{itemize \beameritemnestingprefix item}%
                \usebeamercolor[tcbcolframe]{itemize \beameritemnestingprefix item}##1}}%
          }%
        }%
      }
  \fi%
  \beamer@cramped%
  \justifying% NEW
  %\raggedright% ORIGINAL
  \beamer@firstlineitemizeunskip%
}
\setlength\leftmargini{1.2\baselineskip}
\setlength\leftmarginii{1.2\baselineskip}
\setlength\leftmarginiii{1.2\baselineskip}

\setbeamertemplate{footline}
{
  \leavevmode%
  \hfill\hbox{%
    \color{myBlack50}\insertpagenumber{} / \insertpresentationendpage%
    \hspace*{2pt}%
  }%
  \vspace*{2pt}%
}
\makeatother

\lstdefinestyle{mystyle}{
  basicstyle=\ttfamily,
  stringstyle=\color{myGreen},
  xleftmargin=0.5\baselineskip,
  xrightmargin=0.75\baselineskip,
  aboveskip=0pt,
  belowskip=0pt,
  numbersep=0\baselineskip,				% Abstand der Zahlen zur Umgebung
  numbers=right,					% Zeilennummerierung: none,left,right
  numberstyle=\tiny\color{tcbcolframe}\transparent{1},	% Style der Zeilennummerierung
  stepnumber=1,						% Schrittgröße der Nummerierung
  numberblanklines=false,
  firstnumber=1,					% Legt Startzeile fest  
  numberfirstline=true,
  inputencoding=utf8,
  extendedchars=true,
  keepspaces=true,
  columns = fixed,
  literate={ß}{{\ss}}1 {ä}{{\"a}}1 {ö}{{\"o}}1 {ü}{{\"u}}1 {Ä}{{\"A}}1
  {Ö}{{\"O}}1 {Ü}{{\"U}}1 {á}{{\'a}}1 {ó}{{\'o}}1 {ú}{{\'u}}1 {Á}{{\'A}}1
  {Ó}{{\'O}}1 {Ú}{{\'U}}1 {{~\%}}{{\%}}1 {{~\@}}{{@}}1 {{~`}}{{\`{}}}1
  {{~'}}{{\textquotesingle}}1 {{~Star}}{{\starred}}1
  {{~optionalStar}}{{\optstarred}}1 {{~starred}}{{\starred}}1
  {{~optstarred}}{{\optstarred}}1 {{~circ}}{{\ttfamily$^\circ$}}1
  {{~ddots}}{{\smash{$\ddots$}}}1 {{~vdots}}{{\smash{$\vdots$}}}1
  {{~super}}{{\blue{Supercalifragilisticexpialigetisch}}}1
  {{stupidendframe}}{{}}1,
% Befehle für lokale Hervorhebung (s = single, l = line, i= Begrenzungszeichen wird nicht angezeigt)
  moredelim = **[is][{\transparent{0.5}\smaller[0.75]}]{//minor|}{|},
  moredelim = **[is][{\transparent{0.5}\smaller[0.75]}]{//minor+}{+},
  moredelim = **[is][\color{myBlack}]{//nocolor\{}{\}},
  moredelim = **[is][\color{white}]{//nocolortitle\{}{\}},
  moredelim = **[is][\color{tcbcolframe}]{//alert\{}{\}},
  moredelim = **[is][\color{myRed}]{//red\{}{\}},
  moredelim = **[is][\color{myBlue}]{//blue\{}{\}},
  moredelim = **[is][\color{myGreen}]{//green\{}{\}},
  moredelim = **[is][\color{myYellow}]{//yellow\{}{\}},
  moredelim = **[is][\color{myBlack50}]{//gray\{}{\}},
  moredelim = **[is][\color{myRed}\itshape]{//redArg\{}{\}},
  moredelim = **[is][\color{myBlue}\itshape]{//blueArg\{}{\}},
  moredelim = **[is][\color{myGreen}\itshape]{//greenArg\{}{\}},
  moredelim = **[is][\color{myYellow}\itshape]{//yellowArg\{}{\}},
  moredelim = **[is][\color{myBlack50}\itshape]{//grayArg\{}{\}},
  moredelim = **[is][\color{myBlack}]{//nocolor|}{|},
  moredelim = **[is][\color{white}]{//nocolortitle|}{|},
  moredelim = **[is][\color{tcbcolframe}]{//alert|}{|},
  moredelim = **[is][\color{myRed}]{//red|}{|},
  moredelim = **[is][\color{myGreen}]{//green|}{|},
  moredelim = **[is][\color{myYellow}]{//yellow|}{|},
  moredelim = **[is][\color{myBlue}]{//blue|}{|},
  moredelim = **[is][\color{myBlack50}]{//gray|}{|},
  moredelim = **[is][\color{myRed}\itshape]{//redArg|}{|},
  moredelim = **[is][\color{myBlue}\itshape]{//blueArg|}{|},
  moredelim = **[is][\color{myGreen}\itshape]{//greenArg|}{|},
  moredelim = **[is][\color{myYellow}\itshape]{//yellowArg|}{|},
  moredelim = **[is][\color{myBlack50}\itshape]{//grayArg|}{|},
  moredelim = **[is][\color{myBlack}]{//nocolor+}{+},
  moredelim = **[is][\color{white}]{//nocolortitle+}{+},
  moredelim = **[is][\color{tcbcolframe}]{//alert+}{+},
  moredelim = **[is][\color{myRed}]{//red+}{+},
  moredelim = **[is][\color{myGreen}]{//green+}{+},
  moredelim = **[is][\color{myYellow}]{//yellow+}{+},
  moredelim = **[is][\color{myBlue}]{//blue+}{+},
  moredelim = **[is][\color{myBlack50}]{//gray+}{+},
  moredelim = **[is][\color{myRed}\itshape]{//redArg+}{+},
  moredelim = **[is][\color{myBlue}\itshape]{//blueArg+}{+},
  moredelim = **[is][\color{myGreen}\itshape]{//greenArg+}{+},
  moredelim = **[is][\color{myYellow}\itshape]{//yellowArg+}{+},
  moredelim = **[is][\color{myBlack50}\itshape]{//grayArg+}{+},
  moredelim = **[is][\bfseries]{**}{**},
  moredelim = **[is][\only<2>{}\only<1>{\transparent{0.0}}]{@@}{@@},
  moredelim = **[is][\only<3>{}\only<1-2>{\transparent{0.0}}]{@@@}{@@@},
  moredelim = **[is][\only<4>{}\only<1-3>{\transparent{0.0}}]{@@@@}{@@@@},
  moredelim = **[is][\color{myGreen}]{<<}{>>},
}

\lstdefinestyle{latex}{
  language=[LaTeX]Tex,					% Programmiersprache
  style=mystyle,
  texcsstyle=*\color{myBlue},				% Style der LaTeX-Befehle
  keywordstyle=\color{myRed},			% Style der LaTeX-Umgebungen
  identifierstyle=\color{myBlack},			% Alles außer backslashs und keywords
  commentstyle=\color{myBlack50}\itshape\smaller[0.5],	% Farbe für Kommentare (mit %)
% Mehr Keywords (aka Befehle) festlegen
  alsoletter={0123456789!;.:},
  moretexcs={
    lipsum,
    maketitle,
    subject,
    title,
    subtitle,
    author,
    logo,
    institute,
    date,
    today,
    part,
    chapter,
    section,
    subsection,
    subsubsection,
    paragraph,
    subparagraph,
    frontmatter,
    mainmatter,
    backmatter,
    appendix,
    tableofcontents,
    setlength,
    geometry,
    newgeometry,
    savegeometry,
    loadgeometry,
    restoregeometry,
    textls,
    bf,
    it,
    tt,
    glqq,
    grqq,
    glq,
    grq,
    flqq,
    frqq,
    flq,
    frq,
    guillemotright,
    guillemotleft,
%     !,
%     :,
%     ;,
    linefill,
    newcolumntype,
    RaggedRight,
    RaggedLeft,
    Centering,
    hskip0pt,
    multirow,
    midrule,
    cmidrule,
    toprule,
    bottomrule,
    rowcolor,
    cellcolor,
    columncolor,
    graphicspath,
    columnbreak,
    captionabove,
    captionbelow,
    includegraphics,
    nofrenchspacing,
    leftmargini,
    lightrulewidth,
    heavyrulewidth,
    square,
    coloneqq,
    eqqcolon,
    nexists,
    varnothing,
    rightleftarrows,
    leftrightarrows,
    leftarrowtail,
    rightarrowtail,
    reflectbox,
    rightsquigarrow,
    looparrowleft,
    looparrowright,
    nleftarrow,
    nrightarrow,
    mathbb,
    mathcal,
    mathfrak,
    text,
    dddot,
    iint,
    iiint,
    iiiint,
    idotsint,
    oiint,
    astrosun,
    leftmoon,
    rightmoon,
    mars,
    venus,
    earth,
    jupiter,
    taurus,
    cancer,
    scorpio,
    left,
    right,
    rdbracket,
    sqbracket,
    abs,
    left.,
    right.,
    LaTeXoverbrace,
    LaTeXunderbrace,
    mysin,
    myexp,
    mylog,
    pmb,
    ell,
    operatorname,
    mathllap,
    mathclap,
    mathrlap,
    clap,
    substack,
    fett,
    Vorl,
    rdb,
    sqb,
    abs,
    Binom,
    underset,
    hypersetup,
    murl,
    iurl,
    href,
    NewDocumentCommand,
    RenewDocumentCommand,
    NewDocumentEnvironment,
    RenewDocumentEnvironment,
    vskip,
    the,
    thesection,
    thesubsection,
    hypersetup,
    url,
    nameref,
    autoref,
    vref,
    super,
    refname,
    autocite,
    supercite,
    footcite,
    shorttitle,
    publisher,
    location,
    keyword,
    addbibresource,
    printbibliography,
    printindex,
    acro,
    acroplural,
    scalebox,
    resizebox,
    rotatebox,
    includepdf,
    definecolor,
    color,
    colorbox,
    fcolorbox,
    colorlet,
    textcolor,
    pagecolor,
    column,
    only,
    visible,
    uncover,
    setbeamercovered,
    alert,
    usetheme,
    usecolortheme,
    usefonttheme,
    setbeamercolor,
    setbeamerfont,
    setbeamertemplate,
    addtobeamertemplate,
    beamertemplatenavigationsymbolsempty,
    lstinputlisting,
    lohead,
    rohead,
    cohead,
    lofoot,
    rofoot,
    cofoot,
    automark,
    leftmark,
    rightmark,
    pagemark,
    KOMAoptions,
    setkomafont,
    tcblower,
    tcbset,
    newtcolorbox,
    renewtcolorbox,
    pause,
    lstinline,
    lstset,
    lstdefinestyle,
    Befehlname,
    vskip,
    hskip,
    ding,
    setlist,
    flushright,
    flushleft,
    minisec,
    enquote,
    recalctypearea,
    selectlanguage,
    nonumber,
    num,
    si,
    SI,
    kilo,
    gram,
    meter,
    cubic,
    square,
    per,
    second,
    coordinate,
    node,
    draw,
    fill,
    filldraw,
  },
  morekeywords={
    csquotes,
    article,
    scrartcl,
    report,
    scrreprt,
    book,
    scrbook,
    letter,
    scrlttr2,
    beamer,
    foils,
    slides,
    revtex4,
    document,
    titlepage,
    multicols,
    quote,
    quotation,
    quoting,
    itemize,
    enumerate,
    description,
    center,
    abstract,
    a4paper,
    a5paper,
    portrait,
    landscape,
    oneside,
    twoside,
    onecolumn,
    twocolumn,
    fontenc,
    babel,
    ngerman,
    german,
    english,
    textcomp,
    lmodern,
    microtype,
    mparhack,
    table,
    tabular,
    colortbl,
    xcolor,
    array,
    tabularx,
    longtable,
    tabu,
    longtabu,
    mathtools,
    amsmath,
    amsfonts,
    amssymb,
    math,
    displaymath,
    equation,
    align,
    gather,
    subequations,
    graphicx,
    figure,
    placeins,
    float,
    subfloat,
    subfig,
    subcaption,
    draft,
    dvips,
    xdvi,
    pdftex,
    tikz,
    pgfplots,
    pstricks,
    qrcode,
    sudoku,
    skak,
    pdfpages,
    wrapfig,
    matrix,
    pmatrix,
    bmatrix,
    Bmatrix,
    xparse,
    hyperref,
    minipage,
    thebibliography,
    biblatex,
    biber,
    thesis,
    online,
    misc,
    acronym,
    frame,
    lstlisting,
    listings,
    verbatim,
    scrlayer,
    scrpage, % Mit Minus dazwischen geht's nicht
    tcolorbox
  },
}

\lstdefinestyle{intext}{
  style=latex,
  stringstyle=,
  keywordstyle=,
  keywordstyle=[2],
  identifierstyle=,
  commentstyle=,
  columns=flexible,
}


\lstdefinestyle{bash}{
  language=bash,					% Programmiersprache
  style=mystyle,
  keywordstyle=\color{myBlue},				% Style der Befehle
  identifierstyle=\color{myBlack},			% Alles außer keywords
  commentstyle=\color{myBlack50},				% Farbe für Kommentare (mit %)
% Mehr Keywords (aka Befehle) festlegen
  alsoletter={-.0123456789},
  morekeywords={
    rm,
    mv,
    cp
    scp,
    ssh,
    mkdir,
    sed,
  },
}

%\lstdefinestyle{file}{
%  basicstyle=\ttfamily,
%  keywordstyle=\color{myRed},
%  identifierstyle=\color{myBlue},
%  commentstyle=\color{myBlue},
%% Mehr Keywords (aka Befehle) festlegen
%  alsoletter={.},
%  morekeywords={
%    .tex,
%    .pdf,
%    .zip,
%    .bib,
%    .log,
%    .toc,
%    .aux,
%    .dvi,
%    .exe,
%  },
%}

\lstdefinestyle{python}{
  language=Python,
  basicstyle=\ttfamily,
  keywordstyle=\color{myBlue},
  identifierstyle=\color{myGreen},
  commentstyle=\color{myBlack!50},
  stringstyle=\color{myRed},
  numberstyle=\color{tcbcolframe}\tiny,
  numbers=right,
  numberblanklines=true,
}

\lstdefinestyle{plain}{
  basicstyle=,
  keywordstyle=,
  identifierstyle=,
  commentstyle=,
  stringstyle=,
  numberstyle=,
  numbers=none,
}

\lstset{style=latex}
\lstnewenvironment{morecode}[1][]{\lstset{style=latex,#1}}{}
\lstnewenvironment{morecode*}[1][]{\lstset{style=latex,showspaces=true,#1}}{}
\newcommand{\code}{\lstinline}
\newcommand{\textcode}{\lstinline[style=intext]}

    \tcbset{
      enhanced, % Schaltet erweiterte Farb- und Abstandseinstellungen frei
      autoparskip,
      boxsep=1mm, % 
      boxrule=0.5mm,
      bottom=1mm, % 
	top=1mm, % 
	left=1mm, % 
	right=1mm, % 
	sidebyside gap=5mm, % Damit es gescheit aussieht muss man hier left + right + 2 * boxsep + boxrule rechnen.
      bottomtitle=0.5mm, % 
	toptitle=0.5mm, % 
	lefttitle=1mm, % 
	righttitle=1mm, % 
      arc=0.25mm, %
      outer arc=0.35mm, %
      width=0.88\linewidth,
      fonttitle=\bfseries, % 
      before title={\killDepth}, % Sorgt dafür, dass alle depths des Titels bei der Höhenbestimmung der Titelbox ignoriert werden.
      before upper app=\setlength\columnsep{1.5\baselineskip},
      togglestuff north/.style n args={3}{
	nobeforeafter,
	valign=center,
	overlay={
	  \node [fill=myBlack50, draw=myBlack80, line width=0.5mm, rectangle, inner sep=1.5mm, rounded corners=1mm, text=white, anchor=north, actions ocg={}{#1}{#2}] at ([yshift=-1.2cm,xshift=-0.05cm]frame.north west) {\rotatebox{90}{\killDepth{\small #3}}};
	}
      },
      togglestuff south/.style n args={3}{
	nobeforeafter,
	valign=center,
	overlay={
	  \node [fill=myBlack50, draw=myBlack80, line width=0.5mm, rectangle, inner sep=1.5mm, rounded corners=1mm, text=white, anchor=south, actions ocg={}{#1}{#2}] at ([yshift=0.5cm,xshift=-0.05cm]frame.south west) {\rotatebox{90}{\killDepth{\small #3}}};
	}
      },
    }

\tcbset{
  frogbox/.style={
    enhanced,colback=green!10,colframe=green!65!black,
    enlarge top by=5.5mm,
    overlay={\foreach \x in {2cm,3.5cm} {
    \begin{scope}[shift={([xshift=\x]frame.north west)}]
    \path[draw=green!65!black,fill=green!10,line width=1mm] (0,0) arc (0:180:5mm);
    \path[fill=black] (-0.2,0) arc (0:180:1mm);
    \end{scope}}}}
}

\tcbset{
  ribbon/.style={overlay app={%
    \path[fill=blue!75!white,draw=blue,double=white!85!blue,
    preaction={opacity=0.6,fill=blue!75!white},
    line width=0.1mm,double distance=0.2mm,
    pattern=fivepointed stars,pattern color=white!75!blue]
    ([xshift=-0.2mm,yshift=-1.02cm]frame.north east)
    -- ++(-1,1) -- ++(-0.5,0) -- ++(1.5,-1.5) -- cycle;}
  }
}

    \renewtcolorbox{block}[2][]{
      colframe=myBlue, % Farbe des Rahmens inklusive Titel
      colback=myBlue10, % Farbe des Hintergrunds
      coltext=myBlack90, % Farbe des Textes
      title={#2},
      #1
    }
    \renewtcolorbox{alertblock}[2][]{
      halign upper=flush center,
      colframe=myRed, % Farbe des Rahmens inklusive Titel
      colback=myRed10, % Farbe des Hintergrunds
      coltext=myBlack90, % Farbe des Textes
      title={#2},
      #1
    }
    \renewtcolorbox{exampleblock}[2][]{
      colframe=myGreen, % Farbe des Rahmens inklusive Titel
      colback=myGreen10, % Farbe des Hintergrunds
      coltext=myBlack90, % Farbe des Textes
      title={#2},
      #1
    }
    \newtcolorbox{milkablock}[2][]{
	halign upper=flush center,
	before upper app=\hypersetup{urlcolor=lightpink},
        colframe=darkmilka, % Farbe des Rahmens inklusive Titel
        colback=milka, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        coltitle=platinum, % Farbe des Textes
	title={#2},
	#1
    }
    \newtcolorbox{evalblock}[2][]{
	halign upper=flush center,
	before upper app=\hypersetup{urlcolor=lightpink},
        colframe=darkmilka, % Farbe des Rahmens inklusive Titel
        colback=milka, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        coltitle=platinum, %Farbe des Texts im Titel
	title={#2},
	#1
    }

    \newtcolorbox{horribleblock}[2][]{
	halign upper=flush center,
	before upper app=\hypersetup{urlcolor=white},
        colframe=blue, % Farbe des Rahmens inklusive Titel
        colback=red, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        coltitle=white, %Farbe des Texts im Titel
	title={#2},
	#1
    }

    \newtcolorbox{niceblock}[2][]{
	halign upper=flush center,
	before upper app=\hypersetup{urlcolor=farbe3},
        colframe=farbe3, % Farbe des Rahmens inklusive Titel
        colback=farbe1, % Farbe des Hintergrunds
        coltext=farbe5, % Farbe des Textes
        coltitle=farbe1, % Farbe des Textes
	title={#2},
	#1
    }
    \newtcolorbox{exerciseblock}[3][]{
	righthand width=0.35\linewidth,
	sidebyside,
        colframe=myBlue, % Farbe des Rahmens inklusive Titel
        colback=myBlue10, % Farbe des Hintergrunds
        coltext=myBlack90, % Farbe des Textes
	title={#2},
	after title={\ifthenelse{\equal{#3}{}}{}{\hfill\killDepth{Output -- Seite #3}}},
	#1
    }
    \newtcolorbox{sectionblock}[1][]{
        colframe=myBlue, % Farbe des Rahmens inklusive Titel
        colback=myBlue, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        bottom=5mm, % 
	  top=5mm, % 
	width=0.7\textwidth,
	fontupper=\huge\bfseries,
	halign upper=center,
	#1
    }
    \newtcolorbox{subsectionblock}[1][]{
        colframe=myBlue80, % Farbe des Rahmens inklusive Titel
        colback=myBlue80, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        bottom=5mm, % 
	  top=5mm, % 
	width=0.7\textwidth,
	fontupper=\huge\bfseries,
	halign upper=center,
	#1
    }
    \newtcolorbox{contentsblock}[2][]{
        colframe=myBlue, % Farbe des Rahmens inklusive Titel
        colback=myBlue10, % Farbe des Hintergrunds
        coltext=myBlack90, % Farbe des Textes
	top=0.5\baselineskip, % 
	width=0.8\textwidth,
	title={#2},
	#1
    }
    \newtcolorbox{frametitleblock}[1][]{
	before upper app=\hypersetup{hidelinks},
        colframe=myBlue80, % Farbe des Rahmens inklusive Titel
        colback=myBlue80, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
	nobeforeafter,
	boxsep=1mm,
	width=1\textwidth,
	sharp corners,
% 	rounded corners=south,
	#1
    }
    \newtcolorbox{titleblock}[1][]{
        colframe=myBlue, % Farbe des Rahmens inklusive Titel
        colback=myBlue, % Farbe des Hintergrunds
        coltext=white, % Farbe des Textes
        bottom=5mm, % 
	  top=5mm, % 
	width=0.9\textwidth,
	halign upper=center,
	nobeforeafter,
	#1
    }
    \newtcolorbox{authorblock}[1][]{
        colframe=myRed, % Farbe des Rahmens inklusive Titel
        colback=myRed10, % Farbe des Hintergrunds
        coltext=myBlack90, % Farbe des Textes
        bottom=2mm, % 
	  top=2mm, % 
	width=0.8\textwidth,
	halign upper=center,
	nobeforeafter,
	before skip=0pt,
	toprule=0pt,
	sharp corners=north,
	#1
    }
    \newtcolorbox{instituteblock}[1][]{
        colframe=myYellow, % Farbe des Rahmens inklusive Titel
        colback=myYellow10, % Farbe des Hintergrunds
        coltext=myBlack90, % Farbe des Textes
        bottom=1mm, % 
	  top=1mm, % 
	width=0.7\textwidth,
	halign upper=center,
	nobeforeafter,
	before skip=0pt,
	toprule=0pt,
	sharp corners=north,
	#1
    }
    \newtcolorbox{dateblock}[1][]{
        colframe=myGreen, % Farbe des Rahmens inklusive Titel
        colback=myGreen10, % Farbe des Hintergrunds
        coltext=myBlack90, % Farbe des Textes
        bottom=1mm, % 
	  top=1mm, % 
	width=0.8\textwidth,
	halign upper=center,
	nobeforeafter,
	after skip=0pt,
	bottomrule=0pt,
	sharp corners=south,
	#1
    }
    \RenewDocumentEnvironment{columns}{O{2}O{0.88\textwidth}}{%
      \begin{minipage}[c][\height+0.65\baselineskip][t]{#2}%
      \tcbset{box align=center,width=\linewidth}%
      \vskip0.15\baselineskip
      \begin{multicols}{#1}%
    }{%
      \end{multicols}%
      \end{minipage}%
    }%
    \setlength\columnsep{0.5\baselineskip}

    \setbeamersize{text margin left=0pt, text margin right=0pt}

\newcommand{\cmark}{\textcolor{myGreen}{\ding{51}}}
\newcommand{\omark}{\textcolor{myYellow}{$\pmb{\circ}$}}
\newcommand{\xmark}{\textcolor{myRed}{\ding{55}}}

\hypersetup{colorlinks,urlcolor=myRed,linkcolor=myBlack}
\raggedcolumns
\listfiles
\AtBeginDocument{\centering}
